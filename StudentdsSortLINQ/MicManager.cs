﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentdsSortLINQ
{

    static class MicManager
    {
        public static void Shuffle<T>(this List<T> source)
        {
            Random rnd = new Random();
            int    n   = source.Count;
            while (n > 1)
            {
                n--;
                int k    = rnd.Next(n + 1);
                var strr = source[k];
                source[k] = source[n];
                source[n] = strr;
            }
        }

        public static List<Student> CreateRandomStudents(int count)
        {
            if (count == 0)
            {
                return null;
            }
            var    students = new List<Student>(count);
            Random rnd      = new Random();
            for (int i = 0; i < students.Capacity; i++)
            {
                var student = new Student
                              {
                                  Age        = (byte) rnd.Next(15, 45),
                                  Email      = $"A{i}@gmail.com",
                                  Name       = $"A{i}",
                                  Surname    = $"A{i}yan",
                                  University = (Universities) rnd.Next(10),
                                  Mark       = (byte) rnd.Next(100)
                              };
                students.Add(student);
            }

            return students;
        }

        #region LINQ Functions
        public static Dictionary<Universities, List<Student>> GroupByUniversitiesLINQ(List<Student> students)
        {
            return students.GroupBy(g => g.University).ToDictionary(k => k.Key, v => v.Select(s => s).ToList());
        }

        public static Dictionary<Universities, List<Student>> GroupByUniversitiesOrderedLINQ(List<Student> students)
        {
            return students.GroupBy(g => g.University).ToDictionary(k => k.Key, v => v.Select(s => s).OrderBy(o => o.Mark).ToList());
        }
        #endregion

        #region Ordinary Functions
        public static Dictionary<Universities, List<Student>> GroupByUniversities(List<Student> students)
        {
            Dictionary<Universities, List<Student>> dict = new Dictionary<Universities, List<Student>>();

            foreach (var item in students)
            {
                if (!dict.ContainsKey(item.University))
                {
                    dict.Add(item.University, new List<Student>());
                }
                var studentsList = dict[item.University];
                studentsList.Add(item);
            }
            return dict;
        }

        public static Dictionary<Universities, List<Student>> GroupByUniversitiesOrdered(List<Student> students)
        {
            Dictionary<Universities, List<Student>> dict = new Dictionary<Universities, List<Student>>();

            foreach (var item in students)
            {
                if (!dict.ContainsKey(item.University))
                {
                    dict.Add(item.University, new List<Student>());
                }
                var studentsList = dict[item.University];
                studentsList.Add(item);
            }
            foreach (var value in dict.Values)
            {
                StudentsMarkSort(value);
            }
            return dict;
        }

        private static void StudentsMarkSort(List<Student> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                for (int j = 0; j < source.Count - 1; j++)
                {
                    if (source[j].Mark > source[j + 1].Mark)
                    {
                        var temp = source[j];
                        source[j] = source[j + 1];
                        source[j + 1] = temp;
                    }
                }
            }
        }
        #endregion

    }
}
