﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentdsSortLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = MicManager.CreateRandomStudents(20);
            Print(students);
            Console.WriteLine("\n");

            Print(MicManager.GroupByUniversitiesLINQ(students));
            Console.WriteLine("\n");

            Print(MicManager.GroupByUniversities(students));
            Console.WriteLine("\n");

            Print(MicManager.GroupByUniversitiesOrdered(students));
            Console.WriteLine("\n");

            Print(MicManager.GroupByUniversitiesOrderedLINQ(students));
            Console.WriteLine("\n");

            Console.ReadLine();
        }

        public static void Print(List<Student> students)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("************************Students List************************");
            Console.ResetColor();
            Console.Write($"University\tName\tSurname\tMark\tAge\tEmail\n");
            Console.ForegroundColor = ConsoleColor.Green;
            for (int i = 0; i < students.Count; i++)
            {
                Console.Write($"{students[i].University}\t\t{students[i].Name}\t{students[i].Surname}\t{students[i].Mark}\t{students[i].Age}\t{students[i].Email}\n");
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("*************************************************************");
            Console.ResetColor();
        }

        public static void Print(Dictionary<Universities, List<Student>> source)
        {
            Console.WriteLine("*************************Universities************************");
            foreach (var item in source)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine($"{new string('*',31- item.Key.ToString().Length)}{item.Key}{new string('*', 30)}");
                Print(item.Value);
            }
            Console.ResetColor();
            Console.WriteLine("*************************************************************");
        }
    }
}
