﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentdsSortLINQ
{
    public enum Universities
    {
        YSU,        //Yerevan State University (1919)
        NUACA,      //National University of Architecture and Construction of Armenia(1921)
        YSMU,       //Yerevan State Medical University(1922)
        ASPU,       //Armenian State Pedagogical University named after Khachatur Abovian(1922)
        KSCY,       //Komitas State Conservatory of Yerevan(1923)
        ANAU,       //Armenian National Agrarian University(1930)
        NPUA,       //National Polytechnic University of Armenia(1933)
        YBSU,       //Yerevan Brusov State University of Languages and Social Sciences(1935)
        NSL,        //A.I.Alikhanyan National Science Laboratory(postgraduate) (1943)
        YSITC,       //Yerevan State Institute of Theatre and Cinematography(1944)
        ASIPCS      //Armenian State Institute of Physical Culture and Sport(1945)
    }

    class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public byte Mark { get; set; }
        public string Email { get; set; }
        public Universities University { get; set; }
    }
}
